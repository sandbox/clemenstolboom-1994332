<?php

/**
 * Implementation of hook_drush_command().
 */
function dom_tree_drush_command() {
  $items['dom-get'] = array(
    'description' => 'Grabs the given Drupal webpage',
    'arguments' => array(
      'page' => 'Web page',
    ),
    'options' => array(
      'tidy' => 'Clean up HTML first',
      'file' => 'Save as file',
      'html' => 'Just load as HTML',
      'cookie-file' => 'location of the cookie file to use',
      'user' => 'The Drupal User',
    ),
    'examples' => array(
      'drush @drupal.d8 dom-get' => 'Downloads the front page of the given drush alias.',
      'drush @drupal.d8 --tidy dom-get admin' => 'Downloads the /admin page clean up by white space.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_LOGIN,
  );
  return $items;
}

/**
 * Implementation of hook_drush_help().
 */
function dom_tree_drush_help($section) {
  switch ($section) {
    case 'drush:page-get':
      return dt('Downloads a webpage and optional cleans it.');
  }
}

/**
 * We are logged in for the given user or anonymous.
 *
 * @param type $page
 */
function drush_dom_tree_dom_get($page = '') {
  $uri = drush_get_option('uri');
  $url = $uri . '/' . $page;
  drush_log("Fetching: " . $url);
  _drush_dom_tree_download_file($url);
}

function _drush_dom_tree_download_file($url) {
  $html = drush_get_option('html', FALSE);
  $context = _drush_dom_tree_get_context();
  error_reporting('ERROR_REPORTING_DISPLAY_ALL');
  $file = @file_get_contents($url, NULL, $context);
  if (FALSE !== $file) {
    libxml_use_internal_errors($use_errors = TRUE);
    $dom = new DOMDocument();
    //if (drush_get_option('tidy', TRUE)) {
      $dom->preserveWhiteSpace = FALSE;
      $dom->recover = TRUE;
      $dom->formatOutput = TRUE;
    //}
    if ($html) {
      $dom->loadHTML($file);
    }
    else {
      $dom->loadXML($file);
    }
    if (drush_get_option('file')) {
      file_put_contents(drush_get_option('file'), ($html? $dom->saveHTML(): $dom->saveXML()));
    }
    else {
      echo ($html? $dom->saveHTML(): $dom->saveXML());
    }

    $errors = libxml_get_errors();
    if ($errors) {
      $msg = '';
      foreach ($errors as $error) {
        $msg .= '  ' . $error->message . "\n";
      }
      drush_log("Errors while parsing page $url: \n" . $msg, 'error');
    }
  }
  else {
    drush_log("Unable to fetch page $url : " . $http_response_header[0], 'error');
    drush_log(join("\n", $http_response_header));
  }
}

/**
 * Prepare context for file_get_contents().
 *
 * @return type
 */
function _drush_dom_tree_get_context() {
  $headers = array();
  $headers[] = 'User-Agent: Drush (+http://drupal.org/project/drush)';
  $cookie_file = drush_get_option('cookie-file', NULL);
  if ($cookie_file) {
    $cookie = file_get_contents($cookie_file);
    $headers[] = 'Cookie: ' . $cookie;
  }
  else {
    // FIXME the value of session_id() is not in drupal database
    // although we are fully bootstrapped.
    // How can we make sure this is set correctly?
    //_drush_dom_tree_write_session();
    //$session_id = session_id();
    $session_id = _drush_dom_tree_get_session_id();
    if ($session_id) {
      $headers[] = 'Cookie: ' . session_name() . '=' . $session_id;
    }
    else if (drush_get_option('user', FALSE) !== FALSE) {
      drush_log('Make sure to login through a browser to have an active session for user "' . drush_get_option('user') . '"', 'error');
    }
  }
  $opts = array(
    'http' => array(
      'method' => "GET",
      'header' => join("\r\n", $headers) . "\r\n",
    ),
  );
  drush_log("Headers: " . $opts['http']['header']);
  $context = stream_context_create($opts);
  return $context;
}

/**
 * Grab session id from sessions table.
 *
 * @global type $user
 */
function _drush_dom_tree_get_session_id() {
  global $user;
  if ($user->uid) {
    $sql = 'SELECT sid FROM sessions WHERE uid=:uid';
    $result = db_query($sql, array(':uid' => $user->uid));
    $row = $result->fetchObject();
    if ($row) {
      return $row->sid;
    }
  }
}

/**
 * Make current session persistent.
 *
 * @global type $user
 */
function _drush_dom_tree_write_session() {
  global $user;
  $uid = $user->uid;
  $session_id = session_id();
  $record = array(
    'uid' => $uid,
    'sid' => $session_id,
    'ssid' => '',
  );
  drush_log("Writing session for $uid : $session_id");
  // FIXME: this fails to get into the db
  drupal_write_record('sessions', $record, array('sid', 'ssid'));
}
